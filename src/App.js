import React, {useState} from 'react';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import {v4 as uuid} from "uuid";

const itemsFromBackend = [
  { id: uuid(), content: 'Item 1'},
  { id: uuid(), content: 'Item 2'},
  { id: uuid(), content: 'Item 3'},
  { id: uuid(), content: 'Item 4'},
  { id: uuid(), content: 'Item 5'}
];

const columnsFromBackend = [
  { 
      id: uuid(),
      name: 'Todo',
      items: itemsFromBackend
  },
  { 
    id: uuid(),
    name: 'Progress',
    items: []
  },
  { 
    id: uuid(),
    name: 'End',
    items: []
  },
];

const itemStyle =(isDragging) => ({
  userSelect: 'none',
  padding: 16,
  margin: '4px 0 4px 0',
  minHeight: '50px',
  backgroundColor: isDragging ? '#e84848' : '#456C86',
  color: 'white',
  textAlign: 'center'
});

const getItemStyle = (isDragging, draggingOver, draggableStyle) => { 
  const [...droppableColumns] = document.querySelectorAll("div[data-rbd-droppable-id]");
  //console.log(droppableColumns);
  if (isDragging)
  {
  droppableColumns.map((area) => {
    const areaColumn = area.getAttribute("data-rbd-droppable-id");
    console.log(areaColumn);
    if (draggingOver === areaColumn){
      area.style.background = 'Lightgreen';
    } else {
      area.style.background = '#f9d840';
    }
    return null;
  });
  } else
  {
    droppableColumns.map((area) => {
      area.style.background = 'Lightgrey';
    });
  }
  return {
    userSelect: 'none',
  ...draggableStyle
  }
};

const getListStyle = (isDraggingOver) => ({
  background:  isDraggingOver ? 'Lightpurple' : 'Lightgrey',
  padding: 4,
  width: 250,
  minHeight: 500,
  textAlign: 'center'
});

const handOnDragging = (isDragging, draggingOver) => {
  const[...droppableColumns] = document.getElementById("div.column");
  if (isDragging) console.log("Dragging");
  droppableColumns.map((area) => {
    const areaColumn = area.getElementById("col");
    if (draggingOver === areaColumn) {
      areaColumn.style.backgroundColor = 'Lightgreen';
    } else {
      areaColumn.style.backgroundColor = 'Lightblue';
    }
    return null;
  });
}
const onHandleDragEndItem = (result, columns, setColumns) => {
    if (!result.destination) return;
    const {source, destination } = result;
    if (source.droppableId !== destination.droppableId) 
    {
      const sourceColumn = columns[source.droppableId];
      const destColumn = columns[destination.droppableId];
      const sourceItems = [...sourceColumn.items];
      const destItems = [...destColumn.items];
      const [removed] = sourceItems.splice(source.index, 1);
      destItems.splice(destination.index, 0, removed);
          setColumns({
            ...columns,
            [source.droppableId]: {
              ...sourceColumn,
              items: sourceItems
            },
            [destination.droppableId]: {
              ...destColumn,
              items: destItems
            }
          });
        
    } else {
      const column = columns[source.droppableId];
      const copiedItems = [...column.items];
      const [removed] = copiedItems.splice(source.index, 1);
      copiedItems.splice(destination.index, 0, removed);
      setColumns({
        ...columns,
        [source.droppableId]: {
          ...column,
          items: copiedItems
        }
      });
    }
  };


function App() {
  const [columns, setColumns] = useState(columnsFromBackend);
  return (
    <div style={{ display: 'flex', justifyContent: 'center', height: '100%'}}>
      <DragDropContext onDragEnd={result => onHandleDragEndItem(result, columns, setColumns)}>
      {Object.entries(columns).map(([id, column]) =>
      {
        return (
          <container style={{display: 'flex', flexDirection: 'columns', alignItems: 'center', backgroundColor: '#00f8ff'}}>
            <div style={{margin: '35px'}} class="column" >
          <Droppable droppableId={id} key={id} style={{margin: '8px'}}>
            {(provided, snapshot) =>{
              return (
                <div
                  {...provided.droppableProps}
                  ref={provided.innerRef}
                  style={getListStyle(snapshot.isDraggingOver)}
                >
                  <h2>{column.name}</h2>
                  {column.items.map((item, index) => {
                    return (
                      <Draggable key={item.id} draggableId={item.id} index={index} >
                        {(provided, snapshot) =>{
                          return (
                            <div
                              ref={provided.innerRef}
                              {...provided.draggableProps}
                              {...provided.dragHandleProps}
                              style={getItemStyle(
                                      snapshot.isDragging, snapshot.draggingOver,
                                      provided.draggableProps.style
                                    )}
                              >
                                <div style={itemStyle(snapshot.isDragging)}>{item.content}</div>
                              </div>
                          )
                        }}
                      </Draggable>
                    );
                  })}
            </div>
              )
            }}
          </Droppable>
          </div>
          </container>
        )
      })}
      </DragDropContext>
    </div>
  )
}

export default App;
